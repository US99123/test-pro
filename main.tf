terraform {
 required_providers {
   aws = {
     source  = "hashicorp/aws"
     version = "~> 3.0"
   }
 }
}


# Configure the AWS Provider
provider "aws" {
 region = "us-east-1"
}

# Create a VPC
resource "aws_vpc" "terraform-test-vpc" {
 cidr_block = "10.0.0.0/16"

 tags = {
   Name = "terraform-test"
 }
}


resource "aws_subnet" "subnet-1" {
 vpc_id     = "${aws_vpc.terraform-test-vpc.id}"
 cidr_block = "10.0.1.0/24"

 tags = {
   Name = "subnet-1"
 }
}


# variable "namespace" {
#   description = "The project namespace to use for unique resource naming"
#   default     = "LL-TEST"
#   type        = string
# }

# variable "region" {
#   description = "AWS region"
#   default     = "us-east-1"
#   type        = string
# }


# variable "vpc" {
#   type = any
# }


# resource "aws_instance" "ec2_private" {
#   ami           = data.aws_ami.ubuntu.id
#   associate_public_ip_address = false
#   instance_type               = "t2.micro"
#   # key_name                    = var.key_name
#   subnet_id                   = var.vpc.private_subnets[1]
#   vpc_security_group_ids      = [var.sg_priv_id]

#   tags = {
#     "Name" = "${var.namespace}-EC2-PRIVATE"
#   }

# }
# data "aws_availability_zones" "available" {}
#it is for default subnet
# resource "aws_default_subnet" "default_az_us_east_1a" {
#   availability_zone = "us-east-1a"

# }

# resource "aws_subnet" "subnet-1" {
#   vpc_id     = "${aws_vpc.terraform-test-vpc.id}"
#   cidr_block = "10.0.1.0/24"

#   tags = {
#     Name = "subnet-1"
#   }
# }

# resource "aws_subnet" "subnet-2" {
#   vpc_id     = "${aws_vpc.terraform-test-vpc.id}"
#   cidr_block = "10.0.2.0/24"

#   tags = {
#     Name = "subnet-2"
#   }
# }

# #********its charargeble so code is commented************
# # resource "aws_nat_gateway" "nat-subnet-2" {
# #   connectivity_type = "private"
# #   subnet_id         = "${aws_subnet.subnet-2.id}"
# # }

resource "aws_security_group" "prod_web" {
 name        = "test-sg"
 description = "Allow standard http and https ports inbound and everything outbound"

 ingress {
   from_port   = 80
   to_port     = 80
   protocol    = "tcp"
   cidr_blocks = ["0.0.0.0/0"]
 }
 ingress {
   from_port   = 443
   to_port     = 443
   protocol    = "tcp"
   cidr_blocks = ["0.0.0.0/0"]
 }
 egress {
   from_port   = 0
   to_port     = 0
   protocol    = "-1"
   cidr_blocks = ["0.0.0.0/0"]
 }
 ingress {
   from_port   = 22
   to_port     = 22
   protocol    = "tcp"
   cidr_blocks = ["0.0.0.0/0"]
 }

 tags = {
   "Terraform" : "true"
 }
}

data "aws_ami" "ubuntu" {
 most_recent = true

 filter {
   name   = "name"
   values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
 }

 filter {
   name   = "virtualization-type"
   values = ["hvm"]
 }

 owners = ["099720109477"] # Canonical
}



# resource "aws_instance" "test-site" {
#   ami           = data.aws_ami.ubuntu.id
#   instance_type = "t2.micro"
#   associate_public_ip_address = "false"
#   tags = {
#     Name = "test-site-demo"
#   }
# }


# resource "aws_lb" "test-lb" {
#   name               = "test-lb-tf"
#   internal           = false
#   load_balancer_type = "application"
#   # security_groups    = [aws_security_group.lb_sg.id]
#   # subnets            = [for subnet in aws_subnet.public : subnet.id]

#   # enable_deletion_protection = true

#   # access_logs {
#   #   bucket  = aws_s3_bucket.lb_logs.bucket
#   #   prefix  = "test-lb"
#   #   enabled = true
#   # }

#   tags = {
#     Environment = "test-lb-tf"
#   }
# }